package de.svenkroell.footballscoreapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Game game;

    TextView scoreFieldTeamA;
    TextView scoreFieldTeamB;
    TextView yellowCardsTeamA;
    TextView yellowCardsTeamB;
    TextView redCardsTeamA;
    TextView redCardsTeamB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        game = new Game();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initalizeViewElements();
    }

    public void addGoal(View view) {
        int teamId = getTeamId(view);
        Team team = game.getTeamByIndex(teamId);
        team.addGoal();
        updateTeamScore(team.getScore(), teamId);
    }

    private void updateTeamScore(int score, int index) {
        TextView scoreTeam;
        if (index == 0) {
            scoreTeam = scoreFieldTeamA;
        } else {
            scoreTeam = scoreFieldTeamB;
        }
        scoreTeam.setText(String.valueOf(score));
    }

    public void addYellowCard(View view) {
        int teamId = getTeamId(view);
        Team team = game.getTeamByIndex(teamId);
        team.addYellowCard();
        updateYellowCard(team.getYellowCards(), teamId);
    }

    private void updateYellowCard(int cards, int index) {
        TextView teamYellowCards;
        if (index == 0) {
            teamYellowCards = yellowCardsTeamA;
        } else {
            teamYellowCards = yellowCardsTeamB;
        }
        teamYellowCards.setText(String.valueOf(cards));
    }

    public void addRedCard(View view) {
        int teamId = getTeamId(view);
        Team team = game.getTeamByIndex(teamId);
        team.addRedCard();
        updateRedCard(team.getRedCards(), teamId);
    }

    private void updateRedCard(int cards, int index) {
        TextView teamRedCards;
        if (index == 0) {
            teamRedCards = redCardsTeamA;
        } else {
            teamRedCards = redCardsTeamB;
        }
        teamRedCards.setText(String.valueOf(cards));
    }

    public void resetGame(View view) {
        game.resetGame();
        for (int teamIndex = 0; teamIndex < 2; teamIndex++) {
            Team team = game.getTeamByIndex(teamIndex);
            updateYellowCard(team.getYellowCards(), teamIndex);
            updateRedCard(team.getRedCards(), teamIndex);
            updateTeamScore(team.getScore(), teamIndex);
        }
    }

    private void initalizeViewElements() {
        scoreFieldTeamA = (TextView) findViewById(R.id.scoreFieldTeamA);
        scoreFieldTeamB = (TextView) findViewById(R.id.scoreFieldTeamB);
        yellowCardsTeamA = (TextView) findViewById(R.id.yellowCardsTeamA);
        yellowCardsTeamB = (TextView) findViewById(R.id.yellowCardsTeamB);
        redCardsTeamA = (TextView) findViewById(R.id.redCardsTeamA);
        redCardsTeamB = (TextView) findViewById(R.id.redCardsTeamB);

    }

    private int getTeamId(View view) {
        return Integer.parseInt(view.getTag().toString());
    }
}
