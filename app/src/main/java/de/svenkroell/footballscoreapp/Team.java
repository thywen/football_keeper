package de.svenkroell.footballscoreapp;

/**
 * Created by svenk on 31/01/17.
 */
public class Team {
    private int score;
    private int yellowCards;
    private int redCards;

    public Team() {
        resetScores();
    }

    public int getScore() {
        return score;
    }

    public int getYellowCards() {
        return yellowCards;
    }


    public int getRedCards() {
        return redCards;
    }

    public void addGoal() {
        score += 1;
    }

    public void addYellowCard() {
        yellowCards += 1;
    }

    public void addRedCard() {
        redCards += 1;
    }

    public void resetScores() {
        score = 0;
        yellowCards = 0;
        redCards = 0;
    }
}
