package de.svenkroell.footballscoreapp;

/**
 * Created by svenk on 31/01/17.
 */

public class Game {
    private Team[] teams;

    public Game() {
        teams = new Team[] { new Team(), new Team() };
    }

    public Team getTeamByIndex(int index) {
        return teams[index];
    }

    public void resetGame() {
        for (int teamIndex = 0; teamIndex < 2; teamIndex++) {
            Team team = teams[teamIndex];
            team.resetScores();
        }
    }
}
