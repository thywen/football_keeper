package de.svenkroell.footballscoreapp;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by svenk on 31/01/17.
 */

public class TeamTest {
    private Team team;

    @Before
    public void setUp() {
        team = new Team();
    }

    @Test
    public void teamScoreInitializedWithZero() {
        Assert.assertThat(team.getScore(), is(equalTo(0)));
    }

    @Test
    public void teamScoreCanBeIncreased() {
        team.addGoal();
        Assert.assertThat(team.getScore(), is(equalTo(1)));
    }

    @Test
    public void yellowCardsAreInitializedWithZero() {
        Assert.assertThat(team.getYellowCards(), is(equalTo(0)));
    }

    @Test
    public void yellowCardsCanBeIncreased() {
        team.addYellowCard();
        Assert.assertThat(team.getYellowCards(), is(equalTo(1)));
    }

    @Test
    public void redCardsAreInitializedWithZero() {
        Assert.assertThat(team.getRedCards(), is(equalTo(0)));
    }

    @Test
    public void redCardsCanBeIncreased() {
        team.addRedCard();
        Assert.assertThat(team.getRedCards(), is(equalTo(1)));
    }


    @Test
    public void teamScoreCanBeResettet() {
        team.resetScores();
        Assert.assertThat(team.getScore(), is(equalTo(0)));
        Assert.assertThat(team.getYellowCards(), is(equalTo(0)));
        Assert.assertThat(team.getRedCards(), is(equalTo(0)));
    }
}
