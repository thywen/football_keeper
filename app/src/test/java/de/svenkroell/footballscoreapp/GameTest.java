    package de.svenkroell.footballscoreapp;

    import org.junit.Assert;
    import org.junit.Before;
    import org.junit.Test;

    import static org.hamcrest.CoreMatchers.instanceOf;
    import static org.hamcrest.CoreMatchers.is;
    import static org.hamcrest.CoreMatchers.not;
    import static org.hamcrest.core.IsEqual.equalTo;

    /**
     * Created by svenk on 31/01/17.
     */

    public class GameTest {
        private Game game;

        @Before
        public void setUp() {
            game = new Game();
        }

        @Test
        public void gameHasTwoTeams() {
            Assert.assertThat(game.getTeamByIndex(0), is(instanceOf(Team.class)));
            Assert.assertThat(game.getTeamByIndex(1), is(instanceOf(Team.class)));
            Assert.assertThat(game.getTeamByIndex(0), is(not(equalTo(game.getTeamByIndex(1)))));
        }

        @Test
        public void gameCanBeResetted() {
            Team teamA = game.getTeamByIndex(0);
            Team teamB = game.getTeamByIndex(1);
            teamB.addYellowCard();
            teamB.addGoal();
            teamA.addRedCard();

            game.resetGame();

            Assert.assertThat(teamA, is(equalTo(game.getTeamByIndex(0))));
            Assert.assertThat(teamA.getRedCards(), is(equalTo(0)));

            Assert.assertThat(teamB, is(equalTo(game.getTeamByIndex(1))));
            Assert.assertThat(teamB.getScore(), is(equalTo(0)));
            Assert.assertThat(teamB.getYellowCards(), is(equalTo(0)));
        }

    }
